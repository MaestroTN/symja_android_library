package org.matheclipse.core.reflection.system.rules;

import static org.matheclipse.core.expression.F.*;
import org.matheclipse.core.interfaces.IAST;

/**
 * <p>Generated by <code>org.matheclipse.core.preprocessor.RulePreprocessor</code>.</p>
 * <p>See GIT repository at: <a href="https://bitbucket.org/axelclk/symja_android_library">bitbucket.org/axelclk/symja_android_library under the tools directory</a>.</p>
 */
public interface SecRules {
  /**
   * <ul>
   * <li>index 0 - number of equal rules in <code>RULES</code></li>
	 * </ul>
	 */
  final public static int[] SIZES = { 43, 1 };

  final public static IAST RULES = List(
    IInit(Sec, SIZES),
    // Sec(0)=1
    ISet(Sec(C0),
      C1),
    // Sec(Rational(1,12)*Pi)=Sqrt(6)-Sqrt(2)
    ISet(Sec(Times(QQ(1L,12L),Pi)),
      Plus(Negate(CSqrt2),CSqrt6)),
    // Sec(Rational(1,10)*Pi)=Sqrt(2-2/Sqrt(5))
    ISet(Sec(Times(QQ(1L,10L),Pi)),
      Sqrt(Plus(C2,Times(CN2,C1DSqrt5)))),
    // Sec(Rational(1,8)*Pi)=2/Sqrt(2+Sqrt(2))
    ISet(Sec(Times(QQ(1L,8L),Pi)),
      Times(C2,Power(Plus(C2,CSqrt2),CN1D2))),
    // Sec(Rational(1,6)*Pi)=2/Sqrt(3)
    ISet(Sec(Times(QQ(1L,6L),Pi)),
      Times(C2,C1DSqrt3)),
    // Sec(Rational(1,5)*Pi)=Sqrt(5)+(-1)*1
    ISet(Sec(Times(QQ(1L,5L),Pi)),
      Plus(CN1,CSqrt5)),
    // Sec(Rational(1,4)*Pi)=Sqrt(2)
    ISet(Sec(Times(C1D4,Pi)),
      CSqrt2),
    // Sec(Rational(3,10)*Pi)=Sqrt(2+2/Sqrt(5))
    ISet(Sec(Times(QQ(3L,10L),Pi)),
      Sqrt(Plus(C2,Times(C2,C1DSqrt5)))),
    // Sec(Rational(1,3)*Pi)=2
    ISet(Sec(Times(C1D3,Pi)),
      C2),
    // Sec(Rational(2,5)*Pi)=1+Sqrt(5)
    ISet(Sec(Times(QQ(2L,5L),Pi)),
      Plus(C1,CSqrt5)),
    // Sec(Rational(5,12)*Pi)=Sqrt(6)+Sqrt(2)
    ISet(Sec(Times(QQ(5L,12L),Pi)),
      Plus(CSqrt2,CSqrt6)),
    // Sec(Rational(1,2)*Pi)=ComplexInfinity
    ISet(Sec(Times(C1D2,Pi)),
      CComplexInfinity),
    // Sec(Rational(7,12)*Pi)=-Sqrt(6)-Sqrt(2)
    ISet(Sec(Times(QQ(7L,12L),Pi)),
      Plus(Negate(CSqrt2),Negate(CSqrt6))),
    // Sec(Rational(3,5)*Pi)=-1-Sqrt(5)
    ISet(Sec(Times(QQ(3L,5L),Pi)),
      Plus(CN1,Negate(CSqrt5))),
    // Sec(Rational(2,3)*Pi)=-2
    ISet(Sec(Times(QQ(2L,3L),Pi)),
      CN2),
    // Sec(Rational(7,10)*Pi)=-Sqrt(2+2/Sqrt(5))
    ISet(Sec(Times(QQ(7L,10L),Pi)),
      Negate(Sqrt(Plus(C2,Times(C2,C1DSqrt5))))),
    // Sec(Rational(3,4)*Pi)=-Sqrt(2)
    ISet(Sec(Times(QQ(3L,4L),Pi)),
      Negate(CSqrt2)),
    // Sec(Rational(4,5)*Pi)=1-Sqrt(5)
    ISet(Sec(Times(QQ(4L,5L),Pi)),
      Plus(C1,Negate(CSqrt5))),
    // Sec(Rational(5,6)*Pi)=-2/Sqrt(3)
    ISet(Sec(Times(QQ(5L,6L),Pi)),
      Times(CN2,C1DSqrt3)),
    // Sec(Rational(9,10)*Pi)=-Sqrt(2-2/Sqrt(5))
    ISet(Sec(Times(QQ(9L,10L),Pi)),
      Negate(Sqrt(Plus(C2,Times(CN2,C1DSqrt5))))),
    // Sec(Rational(11,12)*Pi)=Sqrt(2)-Sqrt(6)
    ISet(Sec(Times(QQ(11L,12L),Pi)),
      Plus(CSqrt2,Negate(CSqrt6))),
    // Sec(Pi)=-1
    ISet(Sec(Pi),
      CN1),
    // Sec(Rational(13,12)*Pi)=Sqrt(2)-Sqrt(6)
    ISet(Sec(Times(QQ(13L,12L),Pi)),
      Plus(CSqrt2,Negate(CSqrt6))),
    // Sec(Rational(11,10)*Pi)=-Sqrt(2-2/Sqrt(5))
    ISet(Sec(Times(QQ(11L,10L),Pi)),
      Negate(Sqrt(Plus(C2,Times(CN2,C1DSqrt5))))),
    // Sec(Rational(7,6)*Pi)=-2/Sqrt(3)
    ISet(Sec(Times(QQ(7L,6L),Pi)),
      Times(CN2,C1DSqrt3)),
    // Sec(Rational(6,5)*Pi)=1-Sqrt(5)
    ISet(Sec(Times(QQ(6L,5L),Pi)),
      Plus(C1,Negate(CSqrt5))),
    // Sec(Rational(5,4)*Pi)=-Sqrt(2)
    ISet(Sec(Times(QQ(5L,4L),Pi)),
      Negate(CSqrt2)),
    // Sec(Rational(4,3)*Pi)=-2
    ISet(Sec(Times(QQ(4L,3L),Pi)),
      CN2),
    // Sec(Rational(7,5)*Pi)=-1-Sqrt(5)
    ISet(Sec(Times(QQ(7L,5L),Pi)),
      Plus(CN1,Negate(CSqrt5))),
    // Sec(Rational(3,2)*Pi)=ComplexInfinity
    ISet(Sec(Times(QQ(3L,2L),Pi)),
      CComplexInfinity),
    // Sec(Rational(19,12)*Pi)=Sqrt(6)+Sqrt(2)
    ISet(Sec(Times(QQ(19L,12L),Pi)),
      Plus(CSqrt2,CSqrt6)),
    // Sec(Rational(8,5)*Pi)=1+Sqrt(5)
    ISet(Sec(Times(QQ(8L,5L),Pi)),
      Plus(C1,CSqrt5)),
    // Sec(Rational(5,3)*Pi)=2
    ISet(Sec(Times(QQ(5L,3L),Pi)),
      C2),
    // Sec(Rational(17,10)*Pi)=Sqrt(2+2/Sqrt(5))
    ISet(Sec(Times(QQ(17L,10L),Pi)),
      Sqrt(Plus(C2,Times(C2,C1DSqrt5)))),
    // Sec(Rational(7,4)*Pi)=Sqrt(2)
    ISet(Sec(Times(QQ(7L,4L),Pi)),
      CSqrt2),
    // Sec(Rational(9,5)*Pi)=Sqrt(5)+(-1)*1
    ISet(Sec(Times(QQ(9L,5L),Pi)),
      Plus(CN1,CSqrt5)),
    // Sec(Rational(11,6)*Pi)=2/Sqrt(3)
    ISet(Sec(Times(QQ(11L,6L),Pi)),
      Times(C2,C1DSqrt3)),
    // Sec(Rational(19,10)*Pi)=Sqrt(2-2/Sqrt(5))
    ISet(Sec(Times(QQ(19L,10L),Pi)),
      Sqrt(Plus(C2,Times(CN2,C1DSqrt5)))),
    // Sec(Rational(23,12)*Pi)=Sqrt(6)-Sqrt(2)
    ISet(Sec(Times(QQ(23L,12L),Pi)),
      Plus(Negate(CSqrt2),CSqrt6)),
    // Sec(2*Pi)=1
    ISet(Sec(Times(C2,Pi)),
      C1),
    // Sec(I*Infinity)=0
    ISet(Sec(DirectedInfinity(CI)),
      C0),
    // Sec((-1)*I*Infinity)=0
    ISet(Sec(DirectedInfinity(CNI)),
      C0),
    // Sec(ComplexInfinity)=Indeterminate
    ISet(Sec(CComplexInfinity),
      Indeterminate),
    // Sec(Sqrt(x_^2)):=Sec(x)
    ISetDelayed(Sec(Sqrt(Sqr(x_))),
      Sec(x))
  );
}
