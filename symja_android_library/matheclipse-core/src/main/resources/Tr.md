## Tr

```
Tr(matrix)
```

> computes the trace of a `matrix`.
 
See:   
* [Wikipedia - Trace (linear algebra)](http://en.wikipedia.org/wiki/Trace_matrix)  

### Examples
```
>> Tr({{1,2},{3,4}})
5
```
  