## FractionalPart

```
FractionalPart(number)
```

> get the fractional part auf a `number`. 

### Examples

```
>> FractionalPart(1.5)
0.5  
```
 