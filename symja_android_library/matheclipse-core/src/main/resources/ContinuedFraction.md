## ContinuedFraction

```
ContinuedFraction(number)
```
 
> get the continued fraction representation of `number`.


See:  
* [Wikipedia - Continued fraction](http://en.wikipedia.org/wiki/Continued_fraction)

### Examples 
```
>>> ContinuedFraction(45/16)
{2,1,4,3}
```